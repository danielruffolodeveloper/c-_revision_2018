﻿using System;

namespace Classes_Methods
{

    public class Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
                
        }

        public void Move(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public void Move(Point newLocation)
        {
            if (newLocation == null)
                throw new ArgumentNullException("newLocation");
            
            Move(newLocation.X, newLocation.Y);
        

        }


    }


    class Program
    {
        static void Main(string[] args)
        {
            var point = new Point(10, 20);
            point.Move(new Point(40,60));
            Console.WriteLine("point is at ({0},{1})",point.X,point.Y);
            point.Move(new Point(100, 200));
            Console.WriteLine("point is running from the second method overload at ({0},{1})", point.X, point.Y);


        }
    }
}
