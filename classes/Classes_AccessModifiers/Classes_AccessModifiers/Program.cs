﻿using System;

namespace Classes_AccessModifiers
{

    class Person
    {
        private DateTime _birthdate;
        //priivate variable is underlined and cammelcased


        //method
        public void SetBirthdate(DateTime birthdate)
        {
            _birthdate = birthdate;
        }

        public DateTime GetBithdate()
        {
            return _birthdate;
        }

    }




    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person();
            person.SetBirthdate(new DateTime(1982,1,1));
            Console.WriteLine(person.GetBithdate());
        }
    }
}
