﻿using System;

namespace classes_Objects

//Author:Daniel Ruffolo
// Desc: A simple C# script showing the use for objects and classes and passing objects to other methods.
//Here, we simply simulate sending a message from one object to another , sender and reciever

{

    public class Person

    {
        public string Name;
        public void Introduce(string to)
        {
            Console.WriteLine("message sent to {0}, From {1}", to, Name);
        }
        
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            //main program method here --
            var person = new Person();
            person.Name = "B1";
            person.Introduce("B2");
        }
    }
}
